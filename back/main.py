import bottle
from bottle_mongo import MongoPlugin

from utils import JSONEncoder

root = bottle.Bottle(autojson=False)
# bottle.debug(True)

mongoPlugin = MongoPlugin(uri="mongodb://127.0.0.1", db="autos",
                          serverSelectionTimeoutMS=1000)

jsonPlugin = bottle.JSONPlugin(json_dumps=JSONEncoder().encode)


def connect_subapp(subapp_name):
    subapp = __import__(subapp_name).app
    subapp.install(jsonPlugin)
    subapp.install(mongoPlugin)
    root.mount('/' + subapp_name, subapp)


connect_subapp('filter')

if __name__ == "__main__":
    bottle.run(root)
