def replace_name_with_id(mongodb, collection_name, what):
    return [x["_id"] for x in mongodb[collection_name].find({"name": {"$in": what}})]


def json_to_mongo(mongodb, json):
    mongoquery = {}
    for x in json:
        if x == "model":
            json["model"] = replace_name_with_id(mongodb, "models", json["model"])
            mongoquery.update({"model": {"$in": json["model"]}})
        elif x == "brand":
            json[x] = replace_name_with_id(mongodb, "brands", json[x])
            if "model" not in json:
                modelids = [m["_id"] for m in mongodb.models.find({x: {"$in": json[x]}})]

                mongoquery.update({"model": {"$in": modelids}})
        else:
            mongoquery.update({x: {"$gte": json[x][0], "$lte": json[x][1]} if type(json[x][0]) is float else {"$in": json[x]}})

    return mongoquery
