import bottle
import mapper

app = bottle.Bottle()


@app.post('/')
def get_cars(mongodb):
    body = bottle.request.json

    criteries = mapper.json_to_mongo(mongodb, body)

    carsdb = mongodb.complectation.find(criteries)

    cars = []

    for x in carsdb:
        modelid = x["model"]
        model = mongodb.models.find_one({"_id": modelid})
        modelname = model["name"]
        x["model"] = modelname

        brandid = model["brand"]
        brandname = mongodb.brands.find_one({"_id": brandid})
        x["brand"] = brandname["name"]

        cars.append(x)

    return {
        "data": cars
    }


@app.get('/')
def get_filter(mongodb):
    return {
        "data": mongodb.inputs.find()
    }
