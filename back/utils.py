import json
from bson import ObjectId
from datetime import datetime
from pymongo.collection import Cursor


class JSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        elif isinstance(o, datetime):
            return o.isoformat()
        elif isinstance(o, Cursor):
            return list(o)
        return json.JSONEncoder.default(self, o)
