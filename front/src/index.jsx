import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import AdvancedSearchPage from './pages/AdvancedSearchPage';

ReactDOM.render(<AdvancedSearchPage />, document.getElementById('root'));
