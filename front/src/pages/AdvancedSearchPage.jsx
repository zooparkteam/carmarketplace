import React from 'react';

import { Container, Fab, Button, Table, TableBody } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import AddIcon from '@material-ui/icons/Add';
import FilterRow from '../components/FilterRow';
import FiltersDialog from '../components/FiltersDialog';

const useStyles = makeStyles(theme => ({
  addFab: {
    position: 'fixed',
    bottom: theme.spacing(8),
    right: theme.spacing(2)
  },
  searchButton: {
    position: 'fixed',
    bottom: 0,
    left: 0,
    right: 0,
    width: '100%',
    padding: '0.8em',
    fontWeight: 'bold'
  }
}));


function AdvancedSearchPage() {
  const classes = useStyles();

  const data = [
    {"name": "key1", "type": "array", "value": ['седан', 'хечбек', 'лифтбек', 'кабриолет']},
    {"name": "key2", "type": "boolean", "value": true},
    {"name": "key3", "type": "number", "value": [70000, 5000000, 1000]},
    {"name": "key4", "type": "number", "value": [2.5, 7.2, 0.1]}
  ];

  const filters = [
    {"title": "key1"},
    {"title": "key2"},
    {"title": "key3"},
    {"title": "key4"}
  ];

  const [openFiltersDialog, setOpenFiltersDialog] = React.useState(false);

  const handleClickOpenFiltersDialog = () => {
      setOpenFiltersDialog(true);
  };

  const handleCloseFiltersDialog = () => {
      setOpenFiltersDialog(false);
  };

  function handleAddNewFilter() {
    setOpenFiltersDialog(false);

    // recive filter from back and add it into {data}
  }

  return (
    <Container maxWidth="md" id="AdvancedSearchPage">
      <Table>
        <TableBody>
          {data.map(item => <FilterRow key={item.title} filter={item}/>)}
        </TableBody>
      </Table>

      <Button 
        variant="contained"
        color="secondary"
        size="large"
        className={classes.searchButton}
      >
        Поиск
      </Button>

      <Fab
        color="primary"
        className={classes.addFab}
        aria-label="add"
        onClick={handleClickOpenFiltersDialog}
      >
        <AddIcon />
      </Fab>

      <FiltersDialog
        filters={filters}
        open={openFiltersDialog}
        onClose={handleCloseFiltersDialog}
        onEnter={handleAddNewFilter}
      >
      </FiltersDialog>
    </Container>
  );
}

export default AdvancedSearchPage;
