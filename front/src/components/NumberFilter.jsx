import React from 'react';

import { Slider } from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'

const StyledSlider = withStyles({
  thumb: {
    height: 20,
    width: 20,
    marginTop: -10,
    marginLeft: -10,
  },
  active: {},
  valueLabel: {
    left: 'calc(-50% + 5px)',
    top: -22,
    '& *': {
      background: 'transparent',
      color: '#000',
    },
  }
})(Slider);

function NumberFilter({filter}) {
  const [value, setValue] = React.useState(filter.value.slice(0, 2));
  return (
    <StyledSlider
      value={value}
      onChange={(e, newValue) => setValue(newValue)}
      valueLabelDisplay="auto"
      aria-labelledby="range-slider"
      min={filter.value[0]}
      max={filter.value[1]}
      step={filter.value[2]}
      valueLabelDisplay='on'
      valueLabelFormat={v => v.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1\'') }
    />
  );
}

export default NumberFilter;


