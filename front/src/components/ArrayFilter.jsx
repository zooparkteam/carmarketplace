import React from 'react';

import { Checkbox, TextField } from '@material-ui/core'
import { Autocomplete } from '@material-ui/lab'

function BoolFilter({filter}) {
  const [value, setValue] = React.useState();
  return (
    <Autocomplete
        multiple
        options={filter.value}
        renderInput={params => <TextField {...params} variant="outlined" fullWidth />}
    />
  );
}

export default BoolFilter;


