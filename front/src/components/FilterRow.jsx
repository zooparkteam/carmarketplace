import React from 'react';

import { TableRow, TableCell, IconButton } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import BoolFilter from './BoolFilter';
import NumberFilter from './NumberFilter';
import ArrayFilter from './ArrayFilter';

function FilterRow({filter}) {
  let widget;
  switch (filter.type) {
    case "boolean":
      widget = <BoolFilter filter={filter} />;
      break;
    case "number":
      widget = <NumberFilter filter={filter} />;
      break;
    case "array":
      widget = <ArrayFilter filter={filter} />;
      break;
    default:
      widget = <span>Incorrect Type!</span>;
  }

  return (
    <TableRow>
      <TableCell className="first">{filter.name}</TableCell>
      <TableCell>{widget}</TableCell>
      <TableCell>
        <IconButton aria-label="delete">
          <DeleteIcon />
        </IconButton>
      </TableCell>
    </TableRow>
  );
}

export default FilterRow;
