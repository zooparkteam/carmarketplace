import React from 'react';

import { Checkbox, Switch } from '@material-ui/core'

function BoolFilter({filter}) {
  const [value, setValue] = React.useState();
  return (
    <Switch
      checked={value}
      onChange={e => setValue(e.target.checked)}
    />
  );
}

export default BoolFilter;


