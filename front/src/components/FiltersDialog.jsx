import React from 'react';

import {TextField, Button, Dialog, DialogTitle, DialogContent, DialogActions} from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';


function FiltersDialog({filters, open, onClose, onEnter}) {
    return (
        <Dialog
            open={open}
            onClose={onClose}
        >
            <DialogTitle>
                Добавление фильтра
            </DialogTitle>
            <DialogContent>
                <Autocomplete
                    id="combo-box-demo"
                    options={filters}
                    getOptionLabel={option => option.title}
                    style={{ width: "400px", paddingBottom: "64px" }}
                    renderInput={params => (
                        <TextField {...params} label="Поиск фильтра" variant="outlined" fullWidth />
                    )}
                />
                <DialogActions>
                    <Button onClick={onEnter} color="primary">Добавить</Button>
                </DialogActions>
            </DialogContent>
        </Dialog>
    );
}

export default FiltersDialog;